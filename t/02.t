use v6;
use Test;

use Router::Right;
use Router::Right::Submapper;

plan 2;

my %h;
my @a;
my Bool $raise_except;
my $r  = Router::Right.new;
my $sm = Router::Right::Submapper.new(
    parent  => $r,
    name    => 'smclass',
    path    => 'GET /admin',
    options => %(
        payload => %( controller => 'foo' ),
        methods => 'POST',
        call    => sub ($self)
            {
                $self.add(
                    :name('subadmin'),
                    :path('/subadmin'),
                    :payload( %( action => 'subadmin' ) ),
                );
            }
    ),
);

subtest {
    plan 4;
    is($sm.options<methods>, ['GET', 'POST'], 'object methods are ok');
    my $sm2 = $sm.with(
        :name('smwithclass'),
        :path('GET /admin/area/foo/bar'),
        :options(
            %(
                payload => %( controller => 'foo2', action  => 'bar2') ,
                methods => 'POST',
                call    => sub ($self)
                    {
                        $self.add(
                            :name('subadmin2'),
                            :path('/subadmin2'),
                            :payload( %( action => 'subadmin2' ) ),
                        );
                    }
            )
        ),
    );
    ok($sm ~~ Router::Right::Submapper, 'object type test no.1');
    ok($sm2 ~~ Router::Right::Submapper, 'object type test no.2');
    my $sm3 = $sm2.with(
        :name('smwithclass2'),
        :path('GET /admin/area/foo/bar2'),
        :options(
            %(
                payload => %( controller => 'foo3', action => 'bar3' ),
                methods => 'PUT,DELETE',
                call    => sub ($self)
                    {
                        $self.add(
                            :name('subadmin3'),
                            :path('/subadmin3'),
                            :payload( %( action => 'subadmin3' ) ),
                        );
                    }
            )
        ),
    );
    is(
        $sm3.options<methods>.sort,
        ['GET', 'PUT', 'DELETE' ].sort,
        'methods for call back',
    );
}, 'starter test';

subtest {
    plan 8;
    my $p5r = Router::Right.new;
    my %payload = %( controller => 'Admin' );
    my %options = payload => %payload;
    $p5r.with( :name('admin'), :path('/admin'), :options(%options) )
      .add( :name('users'), :path('/users') )
      .add( :name('status'), :path('GET /status') )
    ;
    is-deeply(
        payload(:match($p5r.match('/admin/users'))),
        %payload,
        'applies defaults test no.1',
    );
    is-deeply(
        payload(:match($p5r.match('/admin/status'))),
        %payload,
        'applies defaults test no.2',
    );
    nok( $p5r.match('/foo'), 'applies defaults test no.3' );
    is-deeply(
        $p5r.route('admin_status')<methods>,
        ['GET'],
        'applies defaults test no.4',
    );
    %options = payload => %( controller => 'A' );
    $p5r.with( :name('a'), :path('/a'), :options(%options) )
        .with(:name('b'), :path('/b'))
          .with(:name('c'), :path('/c'))
            .add(:name('show'), :path('/{username}'))
    ;
    ok($p5r.match('/a/b/c/foo'), 'can be nested');
    %options =
        payload => %( controller => 'Admin' ),
        call => sub ($self) {
            $self.add(
                :name('users'),
                :path('/users'),
                :payload( %( action => 'Users' ) ),
            );
        };
    $p5r.with(
        :name('newadmin'),
        :path('/newadmin'),
        :options(%options),
    );
    my %h = payload(:match($p5r.match('/newadmin/users')));
    is-deeply(
        %h,
        %( controller => 'Admin', action => 'Users'),
        'can apply callback',
    );
    $p5r.with(
        :name('moreadmin'),
        :path('/moreadmin'),
        :options( %( payload => %( controller => 'MoreAdmin' ) ) ),
    ).add(
        :name('users'),
        :path('/users'),
        :payload( %( controller => 'User' ) ),
    );
    is-deeply(
        payload(:match($p5r.match('/moreadmin/users'))),
        %( controller => 'User' ),
        'can override controller',
    );
    $p5r.with(
        :name('oncemoreadmin'),
        :path('/oncemoreadmin'),
        :options( %( payload => %( controller => 'OnceMoreAdmin' ) ) ),
    ).add(
        :name('newusers'),
        :path('/newusers'),
        :payload( %( controller => '::NewUsers' ) ),
    );
    is-deeply(
        payload(:match($p5r.match('/oncemoreadmin/newusers'))),
        %( controller => 'OnceMoreAdmin::NewUsers' ),
        'can append controller',
    );
}, 'P5 Router::Right::Submapper tests';

done-testing;

sub payload(Hash :$match) returns Hash {
    my Hash $r = $match;

    $r<details>:delete;

    $r;
}
