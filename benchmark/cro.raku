use Cro;
use Cro::HTTP::Request;
use Cro::HTTP::Router;
use Test;

plan 400;

my @routes;

my $app = route {
    for ^100 -> $index {
        my $path = $index % 2 ?? "/company/careers/$index" !! "/company/$index";
        my ($rpt1, $rpt2, $rpt3) = $path.split(q{/}, :skip-empty);

        get ->$rpt1, $rpt2, $rpt3 {
            response.status = 200;
            response.append-header('Content-type', 'text/html');
            response.set-body(("This is route: /$rpt1/$rpt2/$rpt3").encode('ascii'));
        };

        if !$rpt3 {
            get ->$rpt1, $rpt2 {
                response.status = 200;
                response.append-header('Content-type', 'text/html');
                response.set-body(("This is route: /$rpt1/$rpt2").encode('ascii'));
            };
        }

        @routes.push($path);
    }
}

my $source = Supplier.new;
my $responses = $app.transformer($source.Supply).Channel;

for @routes -> $route {
    $source.emit(Cro::HTTP::Request.new(:method<GET>, :target($route)));
    given $responses.receive -> $r {
        ok $r ~~ Cro::HTTP::Response, 'Route <' ~ $route ~ '> set routes / correctly';
        is $r.status, 200, 'Got 200 response';
        is $r.header('Content-type'), 'text/html', 'Got expected header';
        is-deeply body-text($r), 'This is route: ' ~ $route, 'Got expected body';
    }
}

done-testing;

sub body-text(Cro::HTTP::Response $r) {
    $r.body-byte-stream.list.map(*.decode('utf-8')).join
}
