# Router::Right — advanced URL router in Raku

Framework-agnostic URL routing engine for web applications in [Raku](https://raku.org).

## Synopsys

```raku
use v6.d;
use Router::Right;

my $route = Router::Right.new;
$route.add(
    :name( 'dynaroute' ),
    :path( 'GET /dynamic/{id:<[\d]>+}/{tag}/{item}' ),
    :payload( 'Dynamic#Route' ),
);
if !(
    my %m = $route.match(
        %*ENV<REQUEST_URI> || ~(),
    )
) {
    say $route.error;
}
else {
    say %m.gist;
}
```

## Wiki

Full documentation is available on Raku Router::Right [wiki pages](https://gitlab.com/pheix/router-right-perl6/wikis/home).

## Methods

1. new() — returns a new Router::Right instance;

2. [add()](https://gitlab.com/pheix/router-right-perl6/wikis/add-method) — defines a new route;

3. [match()](https://gitlab.com/pheix/router-right-perl6/wikis/match-method) — attempts to find a route that matches the supplied path (URL);

4. [error()](https://gitlab.com/pheix/router-right-perl6/wikis/error-method) — returns the error code of the last failed match;

5. [allowed_methods()](https://gitlab.com/pheix/router-right-perl6/wikis/allowed_methods-method) — returns the array of allowed methods for a given route;

6. [url()](https://gitlab.com/pheix/router-right-perl6/wikis/url-method) — constructs a URL from the route;

7. [as_string()](https://gitlab.com/pheix/router-right-perl6/wikis/as_string-method) — returns a report of the defined routes, in order of definition;

8. [with()](https://gitlab.com/pheix/router-right-perl6/wikis/with-method) — helper method to share information across multiple routes;

9. [resource()](https://gitlab.com/pheix/router-right-perl6/wikis/resource-method) — adds routes to create, read, update, and delete a given resource;

10. [route()](https://gitlab.com/pheix/router-right-perl6/wikis/route-method) — returns route details hash.

## Examples

1. [Hello, world!](https://gitlab.com/pheix/router-right-perl6/wikis/hello-world-example) — simple router;

2. [Trick with resources](https://gitlab.com/pheix/router-right-perl6/wikis/trick-with-resources-example) — populate resources with nested [with()](https://gitlab.com/pheix/router-right-perl6/wikis/with-method) call;

## License

Raku Router::Right is free and opensource software, so you can redistribute it and/or modify it under the terms of the [Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## Perl5 Router::Right

[Github](https://github.com/mla/router-right)

[Author](mailto:maurice.aubrey@gmail.com)
