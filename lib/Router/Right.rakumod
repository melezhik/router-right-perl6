unit class Router::Right;

use MONKEY-SEE-NO-EVAL;
use Router::Right::Submapper;
use P5quotemeta;

has %.name_index;
has %.route_index;
has Array @.routes;
has       @.stringfyre;
has Regex $.compiledre;
has UInt  $.error;
has UInt  $!match;
has Match $!m_obj;
has UInt  $!m_index;

sub NOT_FOUND          returns UInt { 404 }
sub METHOD_NOT_ALLOWED returns UInt { 405 }

multi method add(
        Str :$name!,
        Str :$path!,
        :%payload!,
        Str :$options
    ) returns Router::Right {
    X::AdHoc.new( payload => 'no payload defined' ).throw if !%payload;

    self!add($name, $path, %payload, $options);

    self;
}

multi method add(
        Str :$name!,
        Str :$path!,
        Str :$payload!,
        Str :$options
    ) returns Router::Right {
    X::AdHoc.new( payload => 'no payload supplied' ).throw if !$payload;

    my %payload = self.parse_payload($payload);

    X::AdHoc.new( payload => 'no payload defined' ).throw if !%payload;

    self!add($name, $path, %payload, $options);

    self;
}

method allowed_methods( Str $arg? ) returns Array {
    my Int $index;
    if $arg {
        my $path;
        if $arg !~~ m/ '/' / {
            my $name = $arg;
            my %rhandle = %!name_index{ $name } or return [];
            $path = %rhandle<path>;
        }
        else {
            $path = $arg;
        }
        $index = %!route_index{ $path }
            if %!route_index{ $path }:exists;
    }
    else {
        $index = $!match if $!match.defined;

    }
    return [] if !$index.defined;
    my $route = @!routes[$index] or
        X::AdHoc.new(
            payload => 'no routes found for index ' ~ $index,
        ).throw;
    my @methods = @$route.map({ $_<methods>.join(q{,}) });
    my @allowed = self.methods( @methods.join(q{,}), Str );
    @allowed;
}

method as_string returns Str {
    my @routes;
    my %max = name => 0, meth => 0, path => 0;
    for @!routes -> $r {
        for @$r -> %h {
            my Str $name = %h<name> || q{};
            my Str $methods = %h<methods>.join(q{,});
            $methods  ||= q{*};
            %max<name>  = max(%max<name>, $name.chars);
            %max<meth>  = max(%max<meth>, $methods.chars);
            %max<path>  = max(%max<path>, %h<path>.chars);
            my $payload = %h<payload>.gist;
            @routes.push( [ $name, $methods, %h<path>, $payload ] );
        }
    }
    my Str $str;
    for @routes -> $r {
        my Str $frmt =
            '%' ~ %max<name> ~
            's %-' ~ %max<meth> ~
            's %-' ~ %max<path> ~ 's %s' ~ "\n";
        $str ~= sprintf( $frmt, @$r );
    }
    $str;
}

method build_route( Str $path ) returns List {
    X::AdHoc.new( payload => 'no route supplied' ).throw if !$path;
    my Int $index = 0;
    my @ret;
    my @route = $path.split(/\{(<-[\}]>+)\}/,:v);
    @route = @route.map({ s:g/<-[\{\}]>+//; });
    my @regexpr;
    my %placeholders;
    my Bool $is_placeholder = False;
    for @route -> $r {
        if ($is_placeholder) {
            X::AdHoc.new(
                payload => 'invalid placeholder ' ~ $r,
            ).throw if $r !~~ /^ (<-[:]>+) ':'? (.*) $/;
            my ($pname, $regexpr) = ($0, $1);
            X::AdHoc.new(
                payload => 'invalid placeholder name ' ~ $pname,
            ).throw if $pname ~~ m/\//;
            my $optional = 0;
            my $pre      = '';
            my $type     = '';
            if ($pname eq '.format') {
                $optional = 1;
                $pname = 'format';
                $regexpr = '<-[.\s/]>+:?' unless $regexpr.chars;
                $pre = '\\.';
                $type = '.';
            } else {
                $regexpr = '<-[/]>+:?' unless $regexpr.chars;
            }
            X::AdHoc.new(
                payload => 'placeholder ' ~ $pname ~ ' redefined',
            ).throw if %placeholders{$pname}++;
            my %rx =
                pname    => $pname,
                regexpr  => $regexpr,
                optional => $optional,
                type     => $type,
            ;
            @route[$index] = %rx;
            my $opt = $optional ?? '?' !! '';
            @regexpr.push(
                '[' ~
                $pre ~
                '$<' ~
                $pname ~
                '>=(' ~
                $regexpr ~
                ')]' ~
                $opt
            );
        }
        else {
            @regexpr.push( quotemeta($r) ) if $r;
        }
        $is_placeholder = !$is_placeholder;
        $index++;
    }
    @ret.push( @route ).push( @regexpr.join(q{}) );
    @ret;
}

method match( Str $path, Str $method? ) returns Hash {
    my %ret;
    my %matched_route;
    my UInt $idx = 0;
    $!error      = Nil;
    $!match      = Nil;
    $!m_obj      = Nil;
    $!m_index    = Nil;
    my $class    = self;

    if $path ~~ $.compiledre {
        return unless $!m_index !~~ Nil;

        my $idx = $!m_index;

        my $route = (@!routes[$idx]);

        for @$route -> %r {
            if $method {
                if (%r<methods>) {
                  %matched_route =
                    %r if (
                        %r<methods>.map({
                            $_ if $method.uc eq $_
                        })
                    ).elems;
                }
                else {
                    %matched_route = %r;
                }
            }
            else {
                %matched_route = $route[0];
            }

            if !%matched_route {
                $!error = METHOD_NOT_ALLOWED;
            }
            else {
                %ret = %matched_route<payload>;

                %ret<details> =
                {
                    index => $idx,
                    path  => $path
                };

                %ret.push($!m_obj.hash) if $!m_obj;

                $!match = $idx;
            }

            last if %ret;
        }
    }

    if !%ret && ( !$!error ) {
        $!error = NOT_FOUND;
    }

    return %ret;
}

method methods( Str $argm, Str $pathm ) returns List {
    my @ret;
    @ret.append( $argm.split(/<[|,]>/,:skip-empty) ) if $argm;
    @ret.append( $pathm.split(/<[|,]>/,:skip-empty) ) if $pathm;
    @(@ret.map({ s:Perl5:g/^\s+|\s+$//; uc $_ }).unique.sort);
}

method parse_payload( Str $payload ) returns Hash {
    my ($controller, $action) =
        $payload ~~
            /'#' / ??
            split( / '#' /, $payload, 2 ) !!
            ($payload, Str);
    my %h =
        ((controller => $controller) if $controller),
        ((action => $action) if $action);
    %h;
}

method resource(
    Str :$member!,
    :%options,
) returns Router::Right {
    X::AdHoc.new(
        payload => 'no resource member supplied for ' ~ self.^name;
    ).throw if !$member;
    my Str $undef;
    my Str $path       = $member;
    my Str $collection = $member ~ '_set';
    if %options<collection>:exists {
        if %options<collection> {
            $collection = $path =  %options<collection>;
        }
        %options<collection>:delete;
    }
    my %opts = %options if %options;
    %opts<call> = sub ($self) {
        $self.add(
            :name($collection),
            :path('GET /' ~ $path ~ '{.format}'),
            :payload( %( action => 'index' ) ),
        );
        $self.add(
            :name($undef),
            :path('POST /' ~ $path ~ '{.format}'),
            :payload( %( action => 'create' ) ),
        );
        $self.add(
            :name('formatted_' ~ $collection),
            :path('GET /' ~ $path ~ '.{format}'),
            :payload( %( action => 'index' ) ),
        );
        $self.add(
            :name('new_' ~ $member),
            :path('GET /' ~ $path ~ '/new{.format}'),
            :payload( %( action => 'new' ) ),
        );
        $self.add(
            :name('formatted_new_' ~ $member),
            :path('GET /' ~ $path ~ '/new.{format}'),
            :payload( %( action => 'new' ) ),
        );
        $self.add(
            :name($member),
            :path('GET /' ~ $path ~ '/{id}{.format}'),
            :payload( %( action => 'show' ) ),
        );
        $self.add(
            :name($undef),
            :path('PUT /' ~ $path ~ '/{id}{.format}'),
            :payload( %( action => 'update' ) ),
        );
        $self.add(
            :name($undef),
            :path('DELETE /' ~ $path ~ '/{id}{.format}'),
            :payload( %( action => 'delete' ) ),
        );
        $self.add(
            :name('formatted_' ~ $member),
            :path('GET /' ~ $path ~ '/{id}.{format}'),
            :payload( %( action => 'show' ) ),
        );
        $self.add(
            :name("edit_$member"),
            :path('GET /'  ~ $path ~ '/{id}{.format}/edit'),
            :payload( %( action => 'edit' ) ),
        );
        $self.add(
            :name('formatted_edit_' ~ $member),
            :path('GET /' ~ $path ~ '/{id}.{format}/edit'),
            :payload( %( action => 'edit' ) ),
        );
    }
    self.with(
        :name(%opts<name> || ''),
        :path(%opts<path> || ''),
        :options( %opts ),
    );
    self;
}

method route( Str $name ) returns Hash {
    X::AdHoc.new( payload => 'no name supplied' ).throw if !$name;
    %!name_index{$name} // {};
}

method setmobj( Match $m, UInt $route_index) returns Bool {
    $!m_obj   = $m;
    $!m_index = $route_index;

    True;
}

method split_route_path( Str $path ) returns List {
    my @ret;
    if $path {
        if !(
            $path ~~ m/^
                \s*
                [ (<-[\/]>+) \s ]?
                ( \/ .* )
                /
        ) {
            X::AdHoc.new(
                payload => 'invalid route path: ' ~ $path
            ).throw;
        }
        else {
            @ret.push(
                $0 ?? ~$0 !! '',
                $1 ?? ~$1 !! '',
            ).map( { $_ if $_ ~~ s:g/\s+$// } );
        }
    }
    @ret;
}

method url( Str $name, Hash $argh ) returns Str {
    X::AdHoc.new( payload => 'no url name supplied' ).throw if !$name;
    my @path;
    my Bool $is_placeholder = False;
    my Array $route;
    if %!name_index{$name} {
        $route = %!name_index{$name}<route>;
    } elsif $name ~~ m/^ '/' / {
        if $name ~~ / '{' / {
            my (@r, Str $regex) = self.build_route( $name );
            $route = @r[0];
        } else {
            if $name ~~ / ( . ** 0..* ) / {
                $route.push( $0 );
            }
        }
    } else {
        X::AdHoc.new(
            payload => 'url name ' ~ $name ~ ' is invalid'
        ).throw;
    }
    for @$route -> $r {
        if !$is_placeholder {
            @path.push( ~($r) );
            $is_placeholder = !$is_placeholder;
            next;
        }
        my $pname = $r<pname>;
        my $pval;
        if $argh{$pname}:exists {
            $pval = $argh{$pname} || '';
            $argh{$pname}:delete;
            $pval ~~ /<{$r<regexpr>}>/ or
                X::AdHoc.new(
                    payload =>  'invalid value for param "' ~ $pname ~ '"' ~
                                ' in url "' ~ $name ~ '": "' ~ $pval ~ '"';
                ).throw;
        }
        else {
            $r<optional> or
                X::AdHoc.new(
                    payload =>  'required param ' ~ $pname ~
                                ' missing from url "' ~ $name ~ '"';
                ).throw;
        }
        $pval ||= '';
        if ( $pname eq 'format' && $r<type> eq '.' && $pval.chars ) {
            $pval = '.' ~ $pval;
        }
        @path.push( ~($pval) );
        $is_placeholder = !$is_placeholder;
    }
    my @query;
    for ($argh.keys) -> $key {
        @query.push( ~($key ~ q{=} ~ $argh{$key}) );
    }
    ~(@path.join(q{}) ~ ( @query ?? q{?} !! q{} ) ~ @query.join(q{&}));
}

method with(
    Str :$name!,
    Str :$path!,
    :%options!,
) returns Router::Right::Submapper {
    Router::Right::Submapper.new(
        parent  => self,
        name    => $name,
        path    => $path,
        options => %options,
    );
}

submethod !add( Str $name, Str $path, %payload, Str $options? ) {
    X::AdHoc.new( payload => 'no route path supplied' ).throw if !$path;

    X::AdHoc.new(
        payload => 'route <' ~ $name ~ '> already defined',
    ).throw if $name && %!name_index{$name};

    my $class  = self;
    my %p = %payload;

    my ($methods, $_path) = self.split_route_path( $path );

    my @methods = self.methods( $options, $methods );
    my $index   = self!group_index( $_path );
    my @route   = self.build_route( $_path );

    my $re = '[' ~ @route[1] ~ sprintf(' $ { $class.setmobj($/, %d); }]', $index);

    my token R { ^ <{$re}> };

    my %rhandle =
        name    => $name,
        path    => $_path,
        payload => %p,
        methods => @methods,
        route   => @route[0],
        regexpr => &R,
        index   => $index,
    ;

    @!routes[$index].push( %rhandle );
    %!name_index{$name} = %rhandle if $name;

    @!stringfyre.push(sprintf("(%s)",$re));

    my $fullre = @!stringfyre.join('|');

    my token FULLRE { ^ <{$fullre}> };

    $!compiledre = &FULLRE;
}

method !group_index( Str $path ) returns UInt {
    X::AdHoc.new( payload => 'no route path supplied' ).throw if !$path;

    if !(%!route_index{$path}:exists) {
        %!route_index{$path} = @!routes.elems;
    }

    return +(%!route_index{$path});
}
