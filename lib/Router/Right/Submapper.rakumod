unit class Router::Right::Submapper;

use P5quotemeta;

has $.head;
has $.parent  is required;
has $.name    is required;
has $.path    is required;
has %.options is required;

multi method new(:$parent!, Str :$name!, Str :$path!, :%options!) {
    my $head;
    if $parent ~~ Router::Right {
        $head = $parent;
    }
    else {
        $head = $parent.head;
    }
    my ( Str $methods, Str $_path ) = $head.split_route_path($path);
    %options<name>    = $name if !%options<name>;
    %options<path>    = $path if !%options<path>;
    %options<methods> = [
        $head.methods( (%options<methods> || Str) , $methods)
    ] if $methods || %options<methods>;
    my $class = self.bless(
        :$parent,
        :$name,
        :$path,
        :%options,
        :$head,
    );
    if %options<call> {
        my $func = %options<call>;
        # $class.options<call>:delete;
        $func($class);
    }
    $class;
}

method add(
    Str :$name!,
    Str :$path!,
    :%payload?,
) returns Router::Right::Submapper {

    my ($methods, $_path) = $!head.split_route_path($path);
    my @m = [
        $!head.methods(
            (
                %!options<methods> ??
                %!options<methods>.join(q{,}) !!
                Str
            ),
            $methods,
        ) if $methods || %!options<methods>;
    ];
    my Str $_n =
        ( $!name, $name ).map({ $_ if $_ }).join(q{_}) if $name;
    my Str $_p = ( $!path, $_path ).join(q{});
    if %payload {
        %!options<payload><action>:delete
            if %!options<payload><action>;
        for %payload.keys -> $key {
            if (
                $key eq 'controller' &&
                %payload{$key} ~~ /^ '::' / &&
                %!options<payload>{$key}
            ) {
                %!options<payload>{$key} ~= %payload{$key};
            }
            else {
                #%!options.gist.say;
                #%!options<payload>.gist.say;
                %!options<payload>{$key} = %payload{$key};
            }
        }
    }
    $!parent.add(
        :name($_n),
        :path($_p),
        :payload(%!options<payload>),
        :options( @m.join(q{,}) ),
    );
    return self;
}

method with (
    Str :$name!,
    Str :$path!,
    :%options?
) returns Router::Right::Submapper {
    # 'with from submapper'.say;
    self.new(
        parent  => self,
        name    => $name,
        path    => $path,
        options => %options || %!options,
    );
}

method resource(
    Str :$member!,
    :%options!,
) returns Router::Right::Submapper {
    X::AdHoc.new(
        payload => 'no resource member supplied for ' ~ self.^name;
    ).throw if !$member;
    my %opts = %options;
    if $!parent ~~ Router::Right::Submapper {
        %opts<name> =
            ($!parent.name) ~
            q{_} ~
            ( %options<name> || %!options<name> );
        %opts<path> =
            ($!parent.path) ~
            ( %options<path> || %!options<path> );
        my $regexr = quotemeta( %!options<payload><controller> );
        %opts<payload><controller> =
            $!parent.options<payload><controller> ~
            (
                %!options<payload><controller> if
                    %opts<payload><controller> !~~ rx/ <{$regexr}> /
            ) ~
            %opts<payload><controller>;
        %opts<methods> =
            (
                ( $!parent.options<methods> ~ q{,} )
                    if $!parent.options<methods>
            ) ~
            ( %options<methods> || %!options<methods> || '' );
    }
    $!parent.resource(
        :member($member),
        :options( %opts ),
    );
    self;
}
